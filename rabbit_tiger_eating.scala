import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

val sc = new SparkContext("local", "Tiger eats Rabbits", new SparkConf())

val users: RDD[(VertexId, (String))] =
  sc.parallelize(Array((1L, ("Rabbit")), (2L, ("Rabbit")),
      (3L, ("Rabbit")), (4L, ("Rabbit")),
      (5L, ("Rabbit")), (6L, ("Tiger")),
      (7L, ("Rabbit")), (8L, ("Rabbit")),
      (9L, ("Rabbit")),  (10L, ("Rabbit")),
      (11L, ("Rabbit")), (12L, ("Rabbit")),
      (13L, ("Rabbit")), (14L, ("Rabbit")),
      (15L, ("Rabbit")), (16L, ("Tiger")),
      (17L, ("Rabbit")), (18L, ("Rabbit")),
      (19L, ("Rabbit")), (20L, ("Rabbit"))))

val relationships: RDD[Edge[String]] =
  sc.parallelize(Array(
      Edge(10L, 9L, "O"), Edge(10L, 8L, "O"),
      Edge(8L, 7L, "O"), Edge(8L, 5L, "O"),
      Edge(5L, 4L, "O"), Edge(5L, 3L, "O"),
      Edge(5L, 2L, "O"), Edge(9L, 6L, "O"),
      Edge(6L, 1L, "O"), Edge(20L, 11L, "O"),
      Edge(20L, 12L, "O"), Edge(20L, 13L, "O"),
      Edge(5L, 13L, "O"), Edge(20L, 14L, "O"),
      Edge(11L, 15L, "O"), Edge(20L, 16L, "O"),
      Edge(14L, 17L, "O"), Edge(1L, 17L, "O"),
      Edge(20L, 18L, "O"))

val graph = Graph(users, relationships)

val eating_graph = graph.pregel(0L)(
  (id, dist, message) => {
    if (dist.equals("Tiger")) {
      (dist + "_" + message)
    } else if (message != 0){
      "Tiger" + "_" + message
    } else {
      dist + "|" + message
    }
  }, 
  triple => {
    if (triple.srcAttr.startsWith("Tiger") && triple.dstAttr.startsWith("Rabbit")) {
      var new_msg = triple.srcAttr.substring(triple.srcAttr.indexOf("_") + 1)
      var new_dist = new_msg.toLong
      Iterator((triple.dstId, new_dist + 1))
    } 
    else if (triple.srcAttr.startsWith("Rabbit") && triple.dstAttr.startsWith("Tiger")) {
      var new_msg = triple.dstAttr.substring(triple.dstAttr.indexOf("_") + 1)
      var new_dist = new_msg.toLong
      Iterator((triple.srcId, new_dist + 1))
    } else {
      Iterator.empty
    }
  },
  (a, b) => math.min(b, a))

eating_graph.vertices.take(10)

sc.stop()
System.exit(0)